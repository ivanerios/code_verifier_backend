"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const HelloRouter_1 = __importDefault(require("./HelloRouter"));
const logger_1 = require("../utils/logger");
let server = (0, express_1.default)();
let rootRouter = express_1.default.Router();
rootRouter.get('/', (req, res) => {
    (0, logger_1.LogInfo)('GET: http://localhost:8000/api/');
    //Send Hello World
    res.send('Welcome to Api Restful Express + TS + Swagger + Mongoose');
});
server.use('/', rootRouter);
server.use('/hello', HelloRouter_1.default);
exports.default = server;
//# sourceMappingURL=index.js.map