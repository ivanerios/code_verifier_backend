import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config();

// Create Express APP

const app: Express = express();
const port: string | number = process.env.PORT || 8000;

// Define the first Route of APP

app.get('/', (req: Request, res: Response) => {
    //Send Hello World
    res.send('Welcome to Api Restful Express + TS + Swagger + Mongoose');
});

app.get('/ejercicio1', (req: Request, res: Response) => {
      res.json({data: {message: 'Goodbye, world'}});
});

app.get('/hello', (req: Request, res: Response) =>{
    req.query;
    req.query.name ? res.json({data: {message: `Hola, ${req.query.name}`}}) : res.json({data: {message: 'Hola, anónimo'}});
})

// Execute APP and Listen Request to PORT

app.listen(port, () => {console.log(`EXPRESS SERVER: Running at http://localhost:${port}`)})