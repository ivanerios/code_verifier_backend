# Explicación del ejercicio 2

## Dependencias instaladas

Dependencias de producción:

- Cors: es un módulo de seguridad que restringe las solicitudes HTTP según so origen.

- Helmet: Modulo de seguridad que añade Headers a las peticiones a fin de hacerlas más dificiles de atacar.

- Mongoose: Modulo encargado de comunicar NodeJs con la base de datos MongoDB.

Dependencias de desarrollo:

- serve: Superconjunto de Javascript que incorpora el tipado fuerte y el uso de interfaces, destinado a hacer el desarrollo en Javascript más ordenado.

- supertest: Empaquetador de módulos para Javascript, destinado a compactar toda una aplicación en un único .js altamente optimizado.

## Scripts creados

- Para este ejercicio no se han creado Scripts nuevos respecto al anterior.