import { BasicResponse, DateResponse } from "./types";
import { IGoodbyeController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

export class GoodbyeController implements IGoodbyeController {

    public async getMessage(name?: string): Promise<DateResponse> {
        LogSuccess('[/api/goodbye] Get Request');

        let today: object = new Date();
        let date: string = today.toString();

        return{
            message: `Goodbye ${name || "world!"}`,
            date: date
        }
    }
    
}