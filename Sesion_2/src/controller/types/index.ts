export type BasicResponse = {
    message: string
}

export type DateResponse = {
    message: string,
    date: string
}

export type ErrorResponse = {
    error: string,
    message: string
}